# Changelog

## Unreleased

### Added

- spip/ecrire#28 : Reprise dans `spip/dump` de `base_dump_meta_name()` depuis `spip/ecrire`

## 2.3.0 - 2024-11-25

### Security

- spip-team/securite#4841 Limiter l’usage de `#ENV**` dans les formulaires.

### Changed

- spip/spip#5460 `style_prive_plugin_dump` sans compilation de SPIP
